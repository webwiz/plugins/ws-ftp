require("dotenv").config();

const http = require("http");
const WebSocketServer = require("websocket").server;
const minimatch = require("minimatch");
const path = require('path')
const fs = require('fs')

const server = new WebSocketServer({
  httpServer: http.createServer().listen(process.env.PORT || 2100),
});

server.on("request", (request) => {
  let errorMessage = "";
  try {
    if (request.host === `${process.env.HOST}:${process.env.PORT}`) {
    } else if (minimatch(request.host, process.env.HOST)) {
    } else {
      errorMessage = "HOST NOT ALLOWED";
      throw errorMessage;
    }

    /**
     * @todo minimatch not working
     */
    // if (minimatch(request.origin, process.env.ORIGIN)) {
    //   console.log( process.env.ORIGIN, request.origin)
    //   errorMessage = "ORIGIN NOT ALLOWED";
    //   throw errorMessage;
    // }

    const requestCredentials = request.httpRequest.headers.authorization.split(
      " "
    )[1];
    const serverCredentials = Buffer.from(
      `${process.env.USERNAME}:${process.env.PASSWORD}`
    ).toString("base64");

    if (requestCredentials !== serverCredentials) {
      errorMessage = "UNAUTHORISED";
      throw errorMessage;
    }

    const connection = request.accept('ftp');
  } catch (err) {
    request.reject(
      401,
      errorMessage !== "" ? `WS-FTP: ${errorMessage}` : undefined
    );
    console.log(request.httpRequest.headers, errorMessage, err);
  }
});
